package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.RandomDice;
import org.reflections.vfs.Vfs;

/**
 * User: your name
 */
public class YourSolver implements Solver<Board> {

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    @Override
    public String get(Board board) {
        this.board = board;
        if (board.isGameOver()) return Direction.RIGHT.toString();

        int snakeSize = board.getSnake().size();

        Point apple = board.getApples().get(0);

        Point head = board.getHead();
        int snakeX = board.getHead().getX();
        int snakeY = board.getHead().getY();

        if (snakeSize < 10) {
            Point stone = board.getStones().get(0);

            if (stone.getX() == apple.getX() || stone.getX() == snakeX) {
                return yFirst(apple, snakeX, snakeY, head, board);
            }

            if (stone.getY() == apple.getY() || stone.getY() == snakeY) {
               return xFirst(apple, snakeX, snakeY, head, board);
            }
        } else {
            return xFirst(apple, snakeX, snakeY, head, board);
        }

        return xFirst(apple, snakeX, snakeY, head, board);

//        return Direction.DOWN.toString();
    }

    private static String xFirst(Point apple, int snakeX, int snakeY, Point head, Board board) {
        if (apple.getY() > snakeY) {
            if (isNextFieldSafe(Direction.UP, head, board)) {
                return Direction.UP.toString();
            } else if (isNextFieldSafe(Direction.LEFT, head, board)) {
                return Direction.LEFT.toString();
            } else {
                return Direction.RIGHT.toString();
            }
        } else if (apple.getY() < snakeY) {
            if (isNextFieldSafe(Direction.DOWN, head, board)) {
                return Direction.DOWN.toString();
            } else if (isNextFieldSafe(Direction.LEFT, head, board)) {
                return Direction.LEFT.toString();
            } else {
                return Direction.RIGHT.toString();
            }
        } else if (apple.getX() > snakeX) {
            if (isNextFieldSafe(Direction.RIGHT, head, board)) {
                return Direction.RIGHT.toString();
            } else if (isNextFieldSafe(Direction.UP, head, board)) {
                return Direction.UP.toString();
            } else {
                return Direction.DOWN.toString();
            }
        } else {
            return Direction.LEFT.toString();
        }
    }

    private static String yFirst(Point apple, int snakeX, int snakeY, Point head, Board board) {
        if (apple.getX() > snakeX) {
            if (isNextFieldSafe(Direction.RIGHT, head, board)) {
                return Direction.RIGHT.toString();
            } else if (isNextFieldSafe(Direction.UP, head, board)) {
                return Direction.UP.toString();
            } else {
                return Direction.DOWN.toString();
            }

        } else if (apple.getX() < snakeX) {
            if (isNextFieldSafe(Direction.LEFT, head, board)) {
                return Direction.LEFT.toString();
            } else if (isNextFieldSafe(Direction.UP, head, board)) {
                return Direction.UP.toString();
            } else {
                return Direction.DOWN.toString();
            }

        } else if (apple.getY() > snakeY) {
            if (isNextFieldSafe(Direction.UP, head, board)) {
                return Direction.UP.toString();
            } else {
                return Direction.DOWN.toString();
            }
        } else {
            return Direction.DOWN.toString();
        }
    }

    private static boolean isNextFieldSafe(Direction direction, Point head, Board board) {
        switch (direction) {
            case RIGHT:
                return board.getAt(head.getX() + 1, head.getY()).ch() == ' ' || board.getAt(head.getX() + 1, head.getY()).ch() == '☺';
            case DOWN:
                return board.getAt(head.getX(), head.getY() - 1).ch() == ' ' || board.getAt(head.getX(), head.getY() - 1).ch() == '☺';
            case LEFT:
                return board.getAt(head.getX() - 1, head.getY()).ch() == ' ' || board.getAt(head.getX() - 1, head.getY()).ch() == '☺';
            case UP:
                return board.getAt(head.getX(), head.getY() + 1).ch() == ' ' || board.getAt(head.getX(), head.getY() + 1).ch() == '☺';
            default:
                return true;
        }
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                "http://167.71.33.121/codenjoy-contest/board/player/g2hbcrkzzpgbcfneabs1?code=7937625153556397359",
                new YourSolver(new RandomDice()),
                new Board());
    }

}
